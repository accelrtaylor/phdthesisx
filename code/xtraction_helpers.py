import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.colors import Normalize as Norm
import matplotlib as mpl
import numpy as np
import pandas as pd

import os

# Xsuite packages
import xtrack as xt
import xpart as xp
import xobjects as xo

def qse_tune(madx, qx, quad_name):
    madx.input(f'''
    use, sequence=he_ring;
    MATCH;
    GLOBAL, Q1={qx};
    vary, name={quad_name}, step=0.0000001;
    simplex, calls=5000, tolerance=1E-10;
    endmatch;''')
    kqse = madx.eval(quad_name)
    return kqse

def plot_two_twiss(twiss):
    fig = plt.figure(figsize=(12, 12))
    gs = mpl.gridspec.GridSpec(5, 1, height_ratios=[1, 3, 3, 3, 3])
    ax1 = fig.add_subplot(gs[0])
    ax2 = fig.add_subplot(gs[1], sharex=ax1)
    ax3 = fig.add_subplot(gs[2], sharex=ax1)
    ax4 = fig.add_subplot(gs[3], sharex=ax1)

    plt.setp(ax1.get_xticklabels(), visible=False)
    plt.setp(ax2.get_xticklabels(), visible=False)

    # top plot is synoptic
    ax1.axis('off')
    ax1.set_ylim(-1.2, 1)
    ax1.plot([0, twiss['s'].max()], [0, 0], 'k-')

    for _, row in Twiss[Twiss['keyword'].str.contains('quadrupole|rbend|sbend|hkicker')].iterrows():
        if row['keyword'] == 'quadrupole':
            _ = ax1.add_patch(
                mpl.patches.Rectangle(
                    (row['s']-row['l'], 0), row['l'], np.sign(row['k1l']),
                    facecolor='k', edgecolor='k'))
        elif (row['keyword'] == 'rbend' or 
            row['keyword'] == 'sbend'):
            _ = ax1.add_patch(
                mpl.patches.Rectangle(
                    (row['s']-row['l'], -1), row['l'], 2,
                    facecolor='None', edgecolor='k'))
        elif (row['keyword'] == 'hkicker'):
            _ = ax1.add_patch(
                mpl.patches.Rectangle(
                    (row['s']-row['l'], -1), row['l'], 2,
                    facecolor='k', edgecolor='k', alpha=0.5))  

    #2nd plot is beta functions
    ax2.set_ylabel(r'$\beta$ (m)')
    ax2.plot(twiss['s'], twiss['betx'], 'r-', alpha=0.5, label='MADX Twiss X')
    ax2.plot(twiss['s'], twiss['bety'], 'b-', alpha=0.5, label='MADX Twiss Y')

    ax2.legend()

    #2nd plot is beta functions
    ax3.set_ylabel(r'$\alpha$')
    ax3.plot(twiss['s'], twiss['alfx'], 'r-', alpha=0.5)
    ax3.plot(twiss['s'], twiss['alfy'], 'b-', alpha=0.5)
    #3rd plot is dispersion functions
    ax4.set_ylabel('D (m)')
    ax4.plot(twiss['s'], twiss['dx'], 'r-', alpha=0.5)
    ax4.plot(twiss['s'], twiss['dy'], 'b-', alpha=0.5) 

    axnames = ax1.twiny()
    axnames.spines['top'].set_visible(False)
    axnames.spines['left'].set_visible(False)
    axnames.spines['right'].set_visible(False)

    ticks, ticks_labels = list(), list()
    axnames.set_xticks(ticks)
    axnames.set_xticklabels(ticks_labels, rotation=90)

    ax4.set_xlabel('S (m)')

    fig.tight_layout()
    fig.subplots_adjust(hspace=0)
    ax1.set_xlim(twiss['s'].min(), twiss['s'].max())
    plt.show()
    
class MagneticSeptum(xt.BeamElement):
    _xofields = {
        'ano': xo.Float64,
        'cat': xo.Float64,
        'wid': xo.Float64,
        'dpxkick': xo.Float64,
    }
    needs_cpu = True
    _extra_c_sources = ["""
    /*gpufun*/
    void MagneticSeptum_track_local_particle(MagneticSeptumData el, LocalParticle* part0){
        //start_per_particle_block (part0->part)
        if(LocalParticle_get_x(part) >  MagneticSeptumData_get_ano(el)
        && LocalParticle_get_x(part) < (MagneticSeptumData_get_ano(el) + MagneticSeptumData_get_wid(el))){
            LocalParticle_set_state(part, -2);
        }
        if(LocalParticle_get_x(part) > (MagneticSeptumData_get_ano(el) + MagneticSeptumData_get_wid(el))
        && LocalParticle_get_x(part) <  MagneticSeptumData_get_cat(el)){
            float newdpx = LocalParticle_get_px(part) + MagneticSeptumData_get_dpxkick(el);
            //LocalParticle_set_px(part, newdpx);
            LocalParticle_set_state(part, -20);
        }
        if(LocalParticle_get_x(part) > MagneticSeptumData_get_cat(el)){
            LocalParticle_set_state(part, -2);
        }
        //end_per_particle_block
    }
    """]

    def __init__(self, **kwargs):
        if '_xobject' not in kwargs:
            kwargs.setdefault('ano', 85E-3)
            kwargs.setdefault('cat', 85E-3 + 20E-3 + 84E-3)
            kwargs.setdefault('wid', 20E-3)
            kwargs.setdefault('dpxkick', 100E-3)
        super().__init__(**kwargs)
        
class ElectrostaticSeptum(xt.BeamElement):
    _xofields = {
        'ano': xo.Float64,
        'cat': xo.Float64,
        'wid': xo.Float64,
        'dpxkick': xo.Float64,
    }
    needs_cpu = True
    _extra_c_sources = ["""
    /*gpufun*/
    void ElectrostaticSeptum_track_local_particle(ElectrostaticSeptumData el, LocalParticle* part0){
        //start_per_particle_block (part0->part)
        if(LocalParticle_get_x(part) < (ElectrostaticSeptumData_get_ano(el) + ElectrostaticSeptumData_get_wid(el)/2)){
          float newdpx = LocalParticle_get_px(part) + ElectrostaticSeptumData_get_dpxkick(el);
          LocalParticle_set_px(part, newdpx);
          LocalParticle_set_state(part, 20);
          if(ElectrostaticSeptumData_get_dpxkick(el) == 0){
          LocalParticle_set_state(part, -20);
          }
        }
        if(LocalParticle_get_x(part) < ElectrostaticSeptumData_get_cat(el)){
        LocalParticle_set_state(part, -2);
        }
        if(LocalParticle_get_x(part) < (ElectrostaticSeptumData_get_ano(el) - ElectrostaticSeptumData_get_wid(el)/2)
        && LocalParticle_get_x(part) > (ElectrostaticSeptumData_get_ano(el) + ElectrostaticSeptumData_get_wid(el)/2)){
        LocalParticle_set_state(part, -2);
        }
        //end_per_particle_block
    }
    """]

    def __init__(self, **kwargs):
        if '_xobject' not in kwargs:
            kwargs.setdefault('ano', -75E-3)
            kwargs.setdefault('cat', -75E-3 - 21E-3)
            kwargs.setdefault('wid', 0.2E-3)
            kwargs.setdefault('dpxkick', -3.5E-3)
        super().__init__(**kwargs)
        
class BetatronCore(xt.BeamElement):
    _xofields = {
        'dpkick': xo.Float64,
    }
    _extra_c_sources = ["""
        /*gpufun*/
        void BetatronCore_track_local_particle(BetatronCoreData el, LocalParticle* part0){
        //start_per_particle_block (part0->part)
          float newdelta = LocalParticle_get_delta(part) + BetatronCoreData_get_dpkick(el);
          LocalParticle_update_delta(part, newdelta);
        //end_per_particle_block
        }
    """]
