Description of files
------------------------------
| Filename | File Type | Description |
| -------- | --------- | ----------- |
| betatron.str | MADX strength file | a text file containing the strengths needed for the PIMMS.seq lattice.|
| cpymad_PIMMS_Example.ipynb | python notebook | A cpymad example using the PIMMS lattice.|
| fort.18 | text file | Output from running PTC track.|
| internal_mag_pot.txt : | text file | Output from running PTC track.|
| NIMMS_2.33.ipynb | python notebook | An xsuite example of the updated HELiCS lattice .|
| NIMMS_RFKO.ipynb | python notebook | An xsuite example of how to run the RF-KO optimiser function for HeLICS.|
| PIMMS.seq | MADX sequence file | The PIMMS lattice, not including strength values. To be run with cpymad_PIMMS_Example.ipynb.|
| PIMMS_nonlin.ptc | text file | A table containing the output of a PTC_NORMAL function from produced from cpymad_PIMMS_Example.ipynb. It shows 2nd and 3rd order chromaticities.|
| PIMMSTwiss.svg | Scalable Vector Graphics file | A twiss matplotlib figure produced from cpymad_PIMMS_Example.ipynb. Can be opened and edited in Inkscape.|
| track_PIMMS.txtone | text file | A particle tracking data file, produced from cpymad_PIMMS_Example.ipynb|
| xtraction_helpers.py | python file | A series of functions that can be imported to the working notebooks|
