# R. Taylor's Gitlab Compendium to "Slow Extraction: Upgrades for Next Ion Medical Machines with FLASH timescales"
The animations directory contains a series of animations which are referred to and linked to within the pdf of the book. \

The code directory contains a selection of notebooks and python scripts that may be useful to sucessors of NIMMS slow extraction. 
If there is anything missing which you would like included, please email taylor.r@cern.ch \

## To clone this repository to your directory
From the terminal, change directory to where you want the folder to be, then type.\
`clone git@gitlab.com:accelrtaylor/phdthesisx.git` \
You may have to initiate ssh keys, if you have not done so already.\

To update the repository, if you are within the repository.\

`git pull origin main`